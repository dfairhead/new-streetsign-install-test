#!/bin/bash
# setup.sh - useful script for getting a virtualenv etc all set up.
#
####################################################

quit() {
    echo "$1"
    exit 1
}

VDIR=.virtualenv
PYTHON=$VDIR/bin/python
PIP=$VDIR/bin/pip

# You shouldn't need to change these...
VIRTUALENV_VERSION=1.11

VIRTUALENV="$VDIR/virtualenv-$VIRTUALENV_VERSION/virtualenv.py"

####################################################
# Actually start doing stuff:

# if the .virtualenv folder doesn't exist, make it
if [[ ! -d "$VDIR" ]]; then
    echo "Making new folder ($VDIR) for virtualenv to live in."
    mkdir "$VDIR"
fi

# extract virtualenv.py if we need it
if [[ ! -f "$VIRTUALENV" ]]; then
    PACKAGE="virtualenv-$VIRTUALENV_VERSION.tar.gz"
    echo "Downloading VirtualEnv ($VIRTUALENV_VERSION)"
    curl "https://pypi.python.org/packages/source/v/virtualenv/$PACKAGE" -o "$VDIR/$PACKAGE"
    if [[ $? -ne 0 ]]; then
        echo 'Download Failed!'
        exit 1
    fi
    echo "Extracting VirtualEnv..."
    tar -zxC "$VDIR" -f "$VDIR/$PACKAGE"
fi

# if there's no python in virtualenv, make one:
[[ -f "$PYTHON" ]] || python "$VIRTUALENV" "$VDIR"

# get required python modules:

echo "Checking requirements (python modules)"
$PIP install -r "requirements.txt"
if [[ $? -ne 0 ]]; then
    echo 'Something went wrong trying to install the required python modules...'
    exit 2
fi

if [[ ! -f .ssh_key ]]; then
    echo "OK, we need to generate the ansible ssh key now."
    echo "You need to enter a password to encypt it locally, which you'll need"
    echo "again every time you use this tool."
    ssh-keygen -t dsa -f .ssh_key
fi

echo "Cool.  I'll copy the public key to the bootstrap dir..."
ln -s ../.ssh_key.pub playbooks/.ssh_key.pub
echo "Now you should be able to bootstrap the systems to be ready for"
echo "the rest of the provisioning, using:"
echo "./do bootstrap"
