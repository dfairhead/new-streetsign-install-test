#!./.virtualenv/bin/python

PB="./.virtualenv/bin/ansible-playbook"

import sys
from subprocess import call, check_call, CalledProcessError
from os import unlink

##############################################################


def playbook(*vargs):
    ''' call the ansible playbook command '''
    command = [PB, '-c', 'ssh', '-i', 'hosts'] + list(vargs)
    return command

def say(text, status=0):
    ''' attempt to speak out loud, if that fails, then write to stdout '''
    if not text:
        return
    if sys.platform == 'darwin':
        if status:
            call(['say', '-v', 'zarvox', text])
        else:
            call(['say', text])
    else:
        print '-----------'
        print text

##############################################################
# each of these functions is called by running "./do <x>", then do_<x>() is
# called.

def do_bootstrap(address, *vargs):
    ''' Make a machine ready for use with ansible & the rest of our system.

    Expected Arguments:
        <[username@]host> - which machine (and username) to connect to
        <...> - any other arguments you'd like to send to ansible (e.g. -vvvv)'''

    print 'please log in, then quit (to set known_hosts server key...)'
    call(['ssh', address])

    if '@' in address:
        username, host = address.split('@')
    else:
        username = False
        host = address

    command = [PB, '-i', '.bootstrap.inv']

    if username:
        command += ['-u', username]

    with open('.bootstrap.inv', 'w') as f:
        f.write(host)

    def cb(*vargs):
        ''' callback to run after command '''
        unlink('.bootstrap.inv')

    return command + list(vargs) + ['-kK', '-l', host, 'playbooks/bootstrap.yml'], cb


def do_all(*vargs):
    ''' run site.yml, which should include everything. '''

    try:
        check_call(playbook('--syntax-check', 'site.yml'))
    except CalledProcessError:
        say('invalid syntax in play book!', 1)
        exit(2)

    return playbook('site.yml', *vargs), None

def do_pb(filename, *vargs):
    ''' run a playbook.
    Expected Args:
    <filename> of the playbook you want to run.
    <...> any other args you want to send to ansible (such as -vvvv)
    '''

    try:
        check_call(playbook('--syntax-check', filename))
    except CalledProcessError:
        say('invalid syntax in play book!', 1)
        exit(2)

    return playbook(filename, *vargs), None



def do_usage(*vargs):
    print ''' ./do <function> <arguments>
    ----------
    A wrapper for ansible in the virtualenv to make life easier.
    Function Options:
        bootstrap <[user@]address>:
                runs the playbooks/bootstrap.yml script to set up a machine
                ready to have other 'regular' ansible playbooks run on it.
        all
                re-applies your whole site configuration.
        <x.yml>
                run a playbook

    '''
    return None, None

VALID_FUNCTIONS={
    "bootstrap" : do_bootstrap,
    "all": do_all,
    "pb": do_pb,
    "-h": do_usage,
    None: do_usage,
    }

def main(function=None, *vargs):
    ''' delegate to other functions, and display messages or say stuff as
        desired.  Display usage messages & so on when things go wrong. '''

    if function.endswith('.yml'):
        vargs = [function] + list(vargs)
        function = 'pb'

    if function in VALID_FUNCTIONS:
        try:
            command, callback = VALID_FUNCTIONS[function](*vargs)
        except TypeError:
            print 'Invalid invocation:', sys.argv[0], function, vargs
            print '------'
            print function, ':'
            print VALID_FUNCTIONS[function].__doc__
            raise
            exit(1)

        if command:
            print 'Running:'
            print ' '.join(command)
            retcode = call(command)
            if callback:
                text = callback(retcode)
                say(text, retcode)

            if retcode:
                say('failed', 1)
            else:
                say('done!')
            exit(retcode)

        else:
            exit(0)
    else:
        print 'Invalid function: %s' % function
        do_usage()
        exit(1)

if __name__ == '__main__':
    main(*sys.argv[1:])

'''
if [[ $1 == 'bootstrap' ]]; then
    echo $@
    exit 1
    $PB -i hosts -kK playbooks/bootstrap.yml \
    && say "done." && exit
    say -v zarvox "failed" && exit 1
fi

if [[ $1 == "all" ]]; then
    #$PB -i hosts -kK playbooks/bootstrap.yml \
        $PB --private-key=auth.id_rsa -i hosts streetsign.yml \
        && $PB --private-key=auth.id_rsa -i hosts pis.yml \
        && say "done." && exit

    say -v zarvox "failed." && exit 1
fi

if [[ $1 == "ssh" ]]; then
    ssh -i .ssh_key ansible@"$(cat hosts |grep $2 |sed -e 's/.*=\(.*\)/\1/')"
    exit $?
fi

$PB -i hosts $@
if [[ $? -eq 0 ]]; then
    say "done"
else
    say -v zarvox "failed."
fi

'''
