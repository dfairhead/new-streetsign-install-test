# Ansible Install clobber for streetsign on raspberry pis.

To get this ansible setup all ready:

```
    ./setup.sh
```

which will download ansible & other python needed bits into a .virtualenv here

and then

```
    ./do
```

is your friend.

First edit the hosts file to make sure it looks correct for your inventory of machines,
then:

```
    ./do bootstrap <username>@<ip>
```

will set up the machine with an ansible user, log in via ssh keys, etc.

after the machines are bootstrapped, you can do all the rest of the provisioning with:

```
    ./do all
```

which will check all the settings on all the machines and get them all happy.

Read the ansible documentation.  Really.

