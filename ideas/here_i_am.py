#!/usr/bin/python

PORT= 42424

import sys, time

from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_BROADCAST

s = socket(AF_INET, SOCK_DGRAM)
s.bind(('',0))
s.setsockopt(SOL_SOCKET, SO_BROADCAST,1)

while 1:
    data = 'Hi world!'
    s.sendto(data, ('<broadcast>', PORT))
    time.sleep(10)

