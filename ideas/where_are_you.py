PORT = 42424

import sys
from socket import socket, AF_INET, SOCK_DGRAM

s = socket(AF_INET, SOCK_DGRAM)
s.bind(('', PORT))

while 1:
    data, addr = s.recvfrom(1500, 0)
    print addr[0]
