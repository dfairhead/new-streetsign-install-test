#!/bin/bash

cp sneakersync_server.sh /usr/sbin/
cp 99-sneakersync-server.rules /etc/udev/rules.d/

chown root /usr/sbin/sneakersync_server.sh
chown root /etc/udev/rules.d/99-sneakersync-server.rules

chmod oug+rx /usr/sbin/sneakersync_server.sh
chmod oug+rx /etc/udev/rules.d/99-sneakersync-server.rules
