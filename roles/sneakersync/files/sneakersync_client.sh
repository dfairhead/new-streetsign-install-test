#!/bin/bash

if [[ ! -d "/mnt/syncstick" ]]; then
    mkdir /mnt/syncstick
fi

mount /dev/syncstick /mnt/syncstick

sleep 6

rsync -avcW --exclude=.virtualenv /mnt/syncstick/concertino /var/www/ > /var/log/sneakersync.log
