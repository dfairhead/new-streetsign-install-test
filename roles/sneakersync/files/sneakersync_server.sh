#!/bin/bash

if [[ ! -d "/mnt/syncstick" ]]; then
    mkdir /mnt/syncstick
fi

mount /dev/syncstick /mnt/syncstick

rsync -av --exclude .virtualenv /var/www/concertino /mnt/syncstick/concertino > /var/log/sneakersync.log

umount /mnt/syncstick

echo -e '\a' > /dev/console
