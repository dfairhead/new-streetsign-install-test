set nocompatible
set encoding=utf-8
set t_Co=256
filetype off
syntax on

set wildignore+=*.swp,*.pyc

" Python specific bits:
au Filetype python set makeprg=pyflakes\ % 

" Formatting bits:
set ts=4
set shiftwidth=4
set softtabstop=4
set smarttab
set expandtab
set autoindent
set ruler
set incsearch
set hlsearch
set mouse=a
set backspace=indent,eol,start

" Make line numbers fun.
if (v:version >= 703)
    set rnu " relative line numbers
    highlight LineNr ctermbg=darkgrey ctermfg=black
    function! NumberToggle()
        if(&relativenumber == 1)
            set number
        elseif(&number == 1)
            set number!
        else
            set relativenumber
        endif
    endfunc
else
    set number
    function! NumberToggle()
        set number!
    endfunc
endif

map <c-v> "+p
map <c-c> "+y
map <c-x> "+d

" spaces at end of line:
highlight WhitespaceEOL ctermbg=red guibg=red
match WhitespaceEOL /\s\+$/

au FileType python set colorcolumn=81
" set listchars=tab:▸\ ,eol:¬
set laststatus=2
set completeopt=menuone,longest,preview
colorscheme jellybeans


" Other settings:
inoremap <C-space> <C-x><C-o>
map <silent> <F2> :call NumberToggle()<CR>
nnoremap <silent> <F3> :YRShow<CR>
map <silent> <F4> :NERDTreeToggle<CR>
" map <silent> <F5> :TlistToggle<CR>
map <silent> <F6> :w<return>:make<return>
map <silent> <F7> :cnext<return>
map <silent> <F8> :cw<return>
map <silent> <F12> :set invhlsearch<return>
nnoremap ZX :w<return>
" let mapleader=" "

map <silent> <leader>r :rv!<return>
map <silent> <leader>h :set invhlsearch<return>
