#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' simple-redirect/simple.py - (C) 2013 Daniel Fairhead

    ------------------
    A simple localhost-only server which if it can, redirects
    to another site, but if that site is unavailable, returns a sensible
    error message, and retries every so often.
    ------------------

    simple-redirect is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    simple-redirect is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Marlinespike.  If not, see <http://www.gnu.org/licenses/>.


'''


import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import socket

from urllib import urlopen
import BaseHTTPServer
from string import Template
from subprocess import check_output


TITLE= "Simple Redirect"

PORT_NUMBER = 8080

URL = False

try:
    execfile('/etc/simple-redirect.conf')
except:
    pass


TEMPLATE=Template("""<!doctype html>
<html>
<head>
    <title>Simple Redirector</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style>
    body {
        background: black;
        color: white;
        font-family: Sans-Serif, Sans Serif;
        text-align: center;
        padding: 10%;
        overflow: hidden;
    }
    h1 {
        color: white;
        font-size: 50pt;
    }
    p {
        color: #aaa;
        font-size: 30pt;
    }
    </style>
</head>
<body>
    <h1>$title</h1>
    <p>$msg</p>
    <p>IPs:$ips</p>
    <script type="text/javascript">
    setTimeout(function () { location.reload(); }, 5000);
    </script>
</body>
</html>
""")


def get_my_ips():
    output = check_output("ifconfig|awk '/addr:/{print $2}'", shell=True)
    output = check_output("ifconfig|awk '/inet /{gsub(\"addr:\",\"\",$2);print $2}'", shell=True)
    y = [x for x in output.split('\n') if not '127.0.0.1' in x and x]
    return y


    ''' returns a list of this computer's current IP addresses '''
    return (list(set(([ip for ip in
        socket.gethostbyname_ex(socket.gethostname())[2]
        if not ip.startswith("127.")][:1]))))

def can_redirect():
    ''' try and load the config file (in case it's changed), and then try
        to open a connection to the specifiec server, and return the status,
        and a message (either the URL, if successful, or an error message. '''

    try:
        execfile('/etc/simple-redirect.conf')
    except:
        pass


    if URL:
        try:
            urlopen(URL).read()
            return True, URL
        except:
            return (False, "Sorry! I can't connect to the server right now.<br/>"
                          "I'll try again as soon as I can. (%s)" % URL)
    else:
        return False, 'No config file setup.'


class RedirectHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    redir_status = False
    redir_msg = ''

    def redirect(self, url):
        ''' send redirect headers for the specified URL '''

        self.send_response(307)
        self.send_header("Location", url)
        self.end_headers()

    def html_headers(self):
        ''' send the headers for the 'Sorry no can do' page. '''
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()


    def do_HEAD(self):
        self.redir_status, self.redir_msg = can_redirect()
        if self.redir_status:
            print "Redirecting to ", self.redir_msg
            self.redirect(self.redir_msg)
        else:
            self.html_headers()

    def do_GET(self):
        self.do_HEAD()

        if not self.redir_status:
            self.wfile.write(TEMPLATE.substitute(ips=', '.join(get_my_ips()),
                                              msg=self.redir_msg, title=TITLE ))


if __name__ == '__main__':
    server_class = BaseHTTPServer.HTTPServer
    server = server_class(('127.0.0.1', PORT_NUMBER), RedirectHandler)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()

